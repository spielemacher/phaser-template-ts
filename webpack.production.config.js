const path = require('path')
const devConfig = require('./webpack.config')
const DotEnvPlugin = require('dotenv-webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

delete(devConfig.serve)
devConfig.watch = false
devConfig.output.path = path.resolve(__dirname, 'dist')
devConfig.mode = 'production'
devConfig.plugins[0] = new CleanWebpackPlugin([devConfig.output.path])
devConfig.plugins[1] = new DotEnvPlugin({path: './.env-dev'})

devConfig.plugins.push(new CopyWebpackPlugin([{from: 'assets', to: devConfig.output.path + '/assets'}]))

module.exports = devConfig
