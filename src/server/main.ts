import * as SocketIO from 'socket.io'
import {GameServer} from 'src/server/base/GameServer'
import SocketServer from 'src/server/base/SocketServer'

class MyGameServer extends GameServer {

  private socketServer: SocketServer

  constructor(tickTime: number) {
    super(tickTime)
    this.socketServer = new SocketServer(SocketIO.default(8090))
  }

  public update() {
    process.stdout.write('.')
  }

  public async beforeTerminate() {
    await this.socketServer.close()
  }
}

(new MyGameServer(1000)).run()
