"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const SocketIO = __importStar(require("socket.io"));
const GameServer_1 = require("src/server/base/GameServer");
const SocketServer_1 = __importDefault(require("src/server/base/SocketServer"));
class MyGameServer extends GameServer_1.GameServer {
    constructor(tickTime) {
        super(tickTime);
        this.socketServer = new SocketServer_1.default(SocketIO.default(8090));
    }
    update() {
        process.stdout.write('.');
    }
    beforeTerminate() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.socketServer.close();
        });
    }
}
(new MyGameServer(1000)).run();
