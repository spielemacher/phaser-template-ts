"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Command_pb_1 = require("src/shared/protobuf/Command_pb");
class SocketServer {
    constructor(socketIoServer) {
        this.closed = false;
        this.socketIoServer = socketIoServer;
        this._initConnections();
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this.closed = true;
            console.log('SockServ - close called');
            return new Promise((resolve) => {
                console.log('SockServ - promise');
                this.socketIoServer.close(resolve);
            });
        });
    }
    onConnect(socket) {
        if (this.closed) {
            return;
        }
        console.log(`Socket ${socket.id} connected`);
        socket.emit('welcome', 'Hello you funny thingy!');
        setTimeout(() => socket.emit('welcome', 'Another try'), 3000);
        socket.on('command', (data) => {
            const command = Command_pb_1.Command.deserializeBinary(data);
            switch (command.getCommandCase()) {
                case Command_pb_1.Command.CommandCase.REQUEST_IMAGE_CREATE_COMMAND:
                    if (command.hasRequestImageCreateCommand()) {
                        console.log(command.getRequestImageCreateCommand().toObject());
                    }
                    break;
            }
            /*
            if (command.type === 'command') {
              const commandObj = CommandFactory.build(command)
              if (commandObj instanceof RequestImageCreateCommand) {
                socket.broadcast.emit('request', new CreateImageRequest(commandObj.x, commandObj.y))
                socket.emit('request', new CreateImageRequest(commandObj.x, commandObj.y))
              }
            }
            */
            console.log(data);
        });
    }
    _initConnections() {
        this.socketIoServer.on('connection', this.onConnect.bind(this));
    }
}
exports.default = SocketServer;
