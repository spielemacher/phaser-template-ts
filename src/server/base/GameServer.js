"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class GameServer {
    constructor(tickTime) {
        this.interval = null;
        this.lastTickTime = 0;
        this.currentTickTime = 0;
        this.deltaTime = 0;
        this.sigListenersInstalled = false;
        this.terminating = false;
        this.tickTime = tickTime;
    }
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.init();
            if (this.interval) {
                clearInterval(this.interval);
            }
            this.interval = setInterval(((context) => {
                return () => {
                    context.beforeUpdate();
                    context.update(context.deltaTime);
                    context.afterUpdate();
                };
            })(this), this.tickTime);
            this.registerSigListeners();
        });
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            return;
        });
    }
    beforeUpdate() {
        this.currentTickTime = (new Date()).getTime();
        this.deltaTime = this.lastTickTime ? this.currentTickTime - this.lastTickTime : 0;
    }
    afterUpdate() {
        this.lastTickTime = this.currentTickTime;
    }
    beforeTerminate() {
        return __awaiter(this, void 0, void 0, function* () {
            return;
        });
    }
    terminate(signal) {
        return __awaiter(this, void 0, void 0, function* () {
            console.info(`Received Signal ${signal}.`);
            if (this.terminating) {
                console.info('Already terminating, please wait...');
                return;
            }
            this.terminating = true;
            console.info('Start graceful termination of game server');
            if (this.interval) {
                clearInterval(this.interval);
            }
            this.interval = null;
            yield this.beforeTerminate();
            console.info('Game Server terminated gracefully.');
        });
    }
    registerSigListeners() {
        if (!this.sigListenersInstalled) {
            process.on('SIGINT', (sig) => __awaiter(this, void 0, void 0, function* () { return yield this.terminate(sig); }));
            process.on('SIGTERM', (sig) => __awaiter(this, void 0, void 0, function* () { return yield this.terminate(sig); }));
            this.sigListenersInstalled = true;
        }
    }
}
exports.GameServer = GameServer;
