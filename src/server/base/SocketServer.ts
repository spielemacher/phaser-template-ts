import * as SocketIO from 'socket.io'
import {Command} from 'src/shared/protobuf/Command_pb'

export default class SocketServer {

  private socketIoServer: SocketIO.Server
  private closed: boolean = false

  constructor(socketIoServer: SocketIO.Server) {
    this.socketIoServer = socketIoServer
    this._initConnections()
  }

  public async close(): Promise<void> {
    this.closed = true
    return new Promise<void>((resolve) => {
      this.socketIoServer.close(resolve)
    })
  }

  private onConnect(socket: SocketIO.Socket) {
    if (this.closed) {
      return
    }
    console.log(`Socket ${socket.id} connected`)
    socket.emit('welcome', 'Hello you funny thingy!')

    setTimeout(() => socket.emit('welcome', 'Another try'), 3000)

    socket.on('command', (data) => {
      const command = Command.deserializeBinary(data)
      switch (command.getCommandCase()) {
        case Command.CommandCase.REQUEST_IMAGE_CREATE_COMMAND:
          if (command.hasRequestImageCreateCommand()) {

            console.log((command as any).getRequestImageCreateCommand().toObject())
          }
          break
      }
      /*
      if (command.type === 'command') {
        const commandObj = CommandFactory.build(command)
        if (commandObj instanceof RequestImageCreateCommand) {
          socket.broadcast.emit('request', new CreateImageRequest(commandObj.x, commandObj.y))
          socket.emit('request', new CreateImageRequest(commandObj.x, commandObj.y))
        }
      }
      */
      console.log(data)
    })
  }

  private _initConnections() {
    this.socketIoServer.on('connection', this.onConnect.bind(this))
  }
}
