import Signals = NodeJS.Signals

export abstract class GameServer {
  private tickTime: number
  private interval: NodeJS.Timer | null = null
  private lastTickTime: number = 0
  private currentTickTime: number = 0
  private deltaTime: number = 0
  private sigListenersInstalled: boolean = false
  private terminating: boolean = false

  constructor(tickTime: number) {
    this.tickTime = tickTime
  }

  public async run(): Promise<void> {
    await this.init()

    if (this.interval) {
      clearInterval(this.interval)
    }
    this.interval = setInterval(((context) => {
      return () => {
        context.beforeUpdate()
        context.update(context.deltaTime)
        context.afterUpdate()
      }
    })(this), this.tickTime)

    this.registerSigListeners()
  }

  protected async init(): Promise<void> {
    return
  }

  protected beforeUpdate(): void {
    this.currentTickTime = (new Date()).getTime()
    this.deltaTime = this.lastTickTime ? this.currentTickTime - this.lastTickTime : 0
  }

  protected afterUpdate(): void {
    this.lastTickTime = this.currentTickTime
  }

  protected async beforeTerminate(): Promise<void> {
    return
  }

  protected async terminate(signal: Signals): Promise<void> {
    console.info(`Received Signal ${signal}.`)
    if (this.terminating) {
      console.info('Already terminating, please wait...')
      return
    }
    this.terminating = true
    console.info('Start graceful termination of game server')
    if (this.interval) {
      clearInterval(this.interval)
    }
    this.interval = null
    await this.beforeTerminate()
    console.info('Game Server terminated gracefully.')
  }

  protected abstract update(deltaTime: number): void

  private registerSigListeners(): void {
    if (!this.sigListenersInstalled) {
      process.on('SIGINT', async (sig) => await this.terminate(sig))
      process.on('SIGTERM', async (sig) => await this.terminate(sig))
      this.sigListenersInstalled = true
    }
  }
}
