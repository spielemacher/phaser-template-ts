import {RequestImageCreateCommand} from 'src/shared/command/RequestImageCreateCommand'
import {BaseCommand} from 'src/shared/command/BaseCommand'

export class CommandFactory {

  public static build(data: {type: string, subtype: string, [key: string]: any}): BaseCommand | null {
    if (data.type !== 'command') {
      return null
    }

    switch (data.subtype) {
    case 'request-image-create':
      return new RequestImageCreateCommand(data.x, data.y)
    }

    return null
  }

}