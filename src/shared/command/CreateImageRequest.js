"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseRequest_1 = require("src/shared/command/BaseRequest");
class CreateImageRequest extends BaseRequest_1.BaseRequest {
    constructor(x, y) {
        super('create-image');
        this.x = x;
        this.y = y;
    }
}
exports.CreateImageRequest = CreateImageRequest;
