import {BaseCommand} from 'src/shared/command/BaseCommand'

export class RequestImageCreateCommand extends BaseCommand {

  public readonly x: number
  public readonly y: number

  constructor(x: number, y: number) {
    super('request-image-create')
    this.x = x
    this.y = y
  }

}
