import {BaseRequest} from 'src/shared/command/BaseRequest'

export class CreateImageRequest extends BaseRequest {

  public readonly x: number;
  public readonly y: number;

  constructor(x: number, y: number) {
    super('create-image')
    this.x = x
    this.y = y
  }
}
