"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseTransfer_1 = require("src/shared/command/BaseTransfer");
class BaseRequest extends BaseTransfer_1.BaseTransfer {
    constructor(subtype) {
        super('request', subtype);
    }
}
exports.BaseRequest = BaseRequest;
