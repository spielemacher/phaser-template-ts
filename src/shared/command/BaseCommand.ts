import {BaseTransfer} from 'src/shared/command/BaseTransfer'

export class BaseCommand extends BaseTransfer {

  constructor(subtype: string) {
    super('command', subtype)
  }
  
}
