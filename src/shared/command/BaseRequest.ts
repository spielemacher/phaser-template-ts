import {BaseTransfer} from 'src/shared/command/BaseTransfer'

export class BaseRequest extends BaseTransfer {

  constructor(subtype: string) {
    super('request', subtype)
  }

}
