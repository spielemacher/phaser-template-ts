"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const RequestImageCreateCommand_1 = require("src/shared/command/RequestImageCreateCommand");
class CommandFactory {
    static build(data) {
        if (data.type !== 'command') {
            return null;
        }
        switch (data.subtype) {
            case 'request-image-create':
                return new RequestImageCreateCommand_1.RequestImageCreateCommand(data.x, data.y);
        }
        return null;
    }
}
exports.CommandFactory = CommandFactory;
