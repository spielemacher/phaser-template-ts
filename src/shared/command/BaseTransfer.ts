export abstract class BaseTransfer {
  public readonly type: string
  public readonly subtype: string

  constructor(type: string, subtype: string) {
    this.type = type
    this.subtype = subtype
  }
}
