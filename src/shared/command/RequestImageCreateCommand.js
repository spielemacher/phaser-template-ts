"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseCommand_1 = require("src/shared/command/BaseCommand");
class RequestImageCreateCommand extends BaseCommand_1.BaseCommand {
    constructor(x, y) {
        super('request-image-create');
        this.x = x;
        this.y = y;
    }
}
exports.RequestImageCreateCommand = RequestImageCreateCommand;
