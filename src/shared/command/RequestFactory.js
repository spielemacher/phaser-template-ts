import {CreateImageRequest} from 'src/shared/command/CreateImageRequest'

export class RequestFactory {

  static build(data) {
    if (data.type !== 'request') {
      return null
    }

    switch (data.subtype) {
    case 'create-image':
      return new CreateImageRequest(data.x, data.y)
    }

    return null
  }

}
