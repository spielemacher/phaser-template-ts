"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const spielemacher_ecs_1 = require("spielemacher-ecs");
class BusDebugSystem extends spielemacher_ecs_1.System {
    constructor(bus, event) {
        super();
        this.bus = bus;
        this.bus.on('command', console.log);
    }
    test(entity) {
        return false;
    }
    dispose() {
        this.bus.removeListener('command', console.log);
        super.dispose();
    }
}
exports.BusDebugSystem = BusDebugSystem;
