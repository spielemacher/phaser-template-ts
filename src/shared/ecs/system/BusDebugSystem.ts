import {Entity, System} from 'spielemacher-ecs'
import {EventEmitter} from 'events'

export class BusDebugSystem extends System {

  private bus: EventEmitter

  constructor(bus: EventEmitter, event: string) {
    super()
    this.bus = bus
    this.bus.on('command', console.log)
  }

  public test(entity: Entity): boolean {
    return false
  }

  public dispose() {
    this.bus.removeListener('command', console.log)
    super.dispose()
  }
}
