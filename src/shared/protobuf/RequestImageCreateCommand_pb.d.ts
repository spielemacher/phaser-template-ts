// package: io.spielemacher.template.command
// file: src/shared/protobuf/RequestImageCreateCommand.proto

import * as jspb from "google-protobuf";

export class RequestImageCreateCommand extends jspb.Message {
  getX(): number;
  setX(value: number): void;

  getY(): number;
  setY(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RequestImageCreateCommand.AsObject;
  static toObject(includeInstance: boolean, msg: RequestImageCreateCommand): RequestImageCreateCommand.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RequestImageCreateCommand, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RequestImageCreateCommand;
  static deserializeBinaryFromReader(message: RequestImageCreateCommand, reader: jspb.BinaryReader): RequestImageCreateCommand;
}

export namespace RequestImageCreateCommand {
  export type AsObject = {
    x: number,
    y: number,
  }
}

