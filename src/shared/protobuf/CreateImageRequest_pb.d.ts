// package: 
// file: src/shared/protobuf/CreateImageRequest.proto

import * as jspb from "google-protobuf";

export class CreateImageRequest extends jspb.Message {
  getX(): number;
  setX(value: number): void;

  getY(): number;
  setY(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateImageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateImageRequest): CreateImageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateImageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateImageRequest;
  static deserializeBinaryFromReader(message: CreateImageRequest, reader: jspb.BinaryReader): CreateImageRequest;
}

export namespace CreateImageRequest {
  export type AsObject = {
    x: number,
    y: number,
  }
}

