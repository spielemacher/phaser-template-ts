// package: io.spielemacher.template.command
// file: src/shared/protobuf/Command.proto

import * as jspb from "google-protobuf";
import * as src_shared_protobuf_RequestImageCreateCommand_pb from "../../../src/shared/protobuf/RequestImageCreateCommand_pb";

export class Command extends jspb.Message {
  hasRequestImageCreateCommand(): boolean;
  clearRequestImageCreateCommand(): void;
  getRequestImageCreateCommand(): src_shared_protobuf_RequestImageCreateCommand_pb.RequestImageCreateCommand | undefined;
  setRequestImageCreateCommand(value?: src_shared_protobuf_RequestImageCreateCommand_pb.RequestImageCreateCommand): void;

  getCommandCase(): Command.CommandCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Command.AsObject;
  static toObject(includeInstance: boolean, msg: Command): Command.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Command, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Command;
  static deserializeBinaryFromReader(message: Command, reader: jspb.BinaryReader): Command;
}

export namespace Command {
  export type AsObject = {
    requestImageCreateCommand?: src_shared_protobuf_RequestImageCreateCommand_pb.RequestImageCreateCommand.AsObject,
  }

  export enum CommandCase {
    COMMAND_NOT_SET = 0,
    REQUEST_IMAGE_CREATE_COMMAND = 1,
  }
}

