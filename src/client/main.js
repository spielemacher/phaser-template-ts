"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_client_1 = __importDefault(require("socket.io-client"));
const Game_1 = require("src/client/game/Game");
const url = process.env.SERVER_URL || 'http://localhost:8090';
const socket = socket_io_client_1.default(url, { forceNew: true });
socket.on('welcome', console.log);
const game = new Game_1.Game(socket);
window.addEventListener('resize', () => {
    game.resize(window.innerWidth, window.innerHeight);
});
