import {EventEmitter} from 'events'
import Phaser from 'phaser'
import * as SocketIOClient from 'socket.io-client'
import {ECS} from 'spielemacher-ecs'
import {ClickSystem} from 'src/client/ecs/system/ClickSystem'
import {BusDebugSystem} from 'src/shared/ecs/system/BusDebugSystem'
import {ServerCommunicationSystem} from "../ecs/system/ServerCommunicationSystem"

export class DemoScene extends Phaser.Scene {

  private ecs: ECS = new ECS()

  public preload() {
    this.load.image('logo', 'assets/logo.png')
  }

  public create() {
    this.addLogo(400, 150)
    this.resizeToFullScreen()

    const socket: SocketIOClient.Socket = this.registry.get('socketClient')
    const commandBus: EventEmitter = this.registry.get('commandBus')

    this.ecs.addSystem(new ClickSystem(this.input, commandBus))
    this.ecs.addSystem(new BusDebugSystem(commandBus, 'command'))
    this.ecs.addSystem(new ServerCommunicationSystem(socket, commandBus))

  }

  public update(time: number, delta: number): void {
    super.update(time, delta)
    this.ecs.update(delta)
  }

  private resizeToFullScreen() {
    this.events.on('resize', (width: number, height: number) => {
      this.cameras.resize(width, height)
    })
  }

  private addLogo(x: number, y: number) {
    const logo = this.add.image(x, y, 'logo')

    this.tweens.add({
      duration: 2000,
      ease: Phaser.Math.Easing.Elastic.InOut,
      targets: logo,
      y: y + 300,
      yoyo: true,
      loop: -1
    })

  }
}
