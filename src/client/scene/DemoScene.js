"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const phaser_1 = __importDefault(require("phaser"));
const spielemacher_ecs_1 = require("spielemacher-ecs");
const ClickSystem_1 = require("src/client/ecs/system/ClickSystem");
const BusDebugSystem_1 = require("src/shared/ecs/system/BusDebugSystem");
const ServerCommunicationSystem_1 = require("../ecs/system/ServerCommunicationSystem");
class DemoScene extends phaser_1.default.Scene {
    constructor() {
        super(...arguments);
        this.ecs = new spielemacher_ecs_1.ECS();
    }
    preload() {
        this.load.image('logo', 'assets/logo.png');
    }
    create() {
        this.addLogo(400, 150);
        this.resizeToFullScreen();
        const socket = this.registry.get('socketClient');
        const commandBus = this.registry.get('commandBus');
        this.ecs.addSystem(new ClickSystem_1.ClickSystem(this.input, commandBus));
        this.ecs.addSystem(new BusDebugSystem_1.BusDebugSystem(commandBus, 'command'));
        this.ecs.addSystem(new ServerCommunicationSystem_1.ServerCommunicationSystem(socket, commandBus));
    }
    update(time, delta) {
        super.update(time, delta);
        this.ecs.update(delta);
    }
    resizeToFullScreen() {
        this.events.on('resize', (width, height) => {
            this.cameras.resize(width, height);
        });
    }
    addLogo(x, y) {
        const logo = this.add.image(x, y, 'logo');
        this.tweens.add({
            duration: 2000,
            ease: phaser_1.default.Math.Easing.Elastic.InOut,
            targets: logo,
            y: y + 300,
            yoyo: true,
            loop: -1
        });
    }
}
exports.DemoScene = DemoScene;
