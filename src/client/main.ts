import io from 'socket.io-client'
import {Game} from 'src/client/game/Game'

const url = process.env.SERVER_URL || 'http://localhost:8090'
const socket = io(url, {forceNew: true})
socket.on('welcome', console.log)
const game = new Game(socket)

window.addEventListener('resize', () => {
    game.resize(window.innerWidth, window.innerHeight)
})
