import {EventEmitter} from 'events'
import Phaser from 'phaser'
import * as SocketIOClient from 'socket.io-client'
import {DemoScene} from '../scene/DemoScene'

export class Game extends Phaser.Game {

  constructor(socketClient: SocketIOClient.Socket) {
    super({
      type: Phaser.AUTO,
      parent: 'game',
      width: window.innerWidth,
      height: window.innerHeight,
      scene: [
        DemoScene
      ]
    })

    this.registry.set('socketClient', socketClient)
    this.registry.set('commandBus', new EventEmitter())
    this.registry.set('requestBus', new EventEmitter())
  }
}
