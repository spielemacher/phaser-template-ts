"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const phaser_1 = __importDefault(require("phaser"));
const DemoScene_1 = require("../scene/DemoScene");
class Game extends phaser_1.default.Game {
    constructor(socketClient) {
        super({
            type: phaser_1.default.AUTO,
            parent: 'game',
            width: window.innerWidth,
            height: window.innerHeight,
            scene: [
                DemoScene_1.DemoScene
            ]
        });
        this.registry.set('socketClient', socketClient);
        this.registry.set('commandBus', new events_1.EventEmitter());
        this.registry.set('requestBus', new events_1.EventEmitter());
    }
}
exports.Game = Game;
