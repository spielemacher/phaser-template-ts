"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const spielemacher_ecs_1 = require("spielemacher-ecs");
const RequestImageCreateCommand_pb_1 = require("src/shared/protobuf/RequestImageCreateCommand_pb");
class ClickSystem extends spielemacher_ecs_1.System {
    constructor(input, commandBus) {
        super();
        this.input = input;
        this.commandBus = commandBus;
        this.input.on('pointerdown', this.onClick, this);
    }
    test(entity) {
        return false;
    }
    dispose() {
        this.input.removeListener('pointerdown', this.onClick, this, false);
        super.dispose();
    }
    onClick(data) {
        const command = new RequestImageCreateCommand_pb_1.RequestImageCreateCommand();
        command.setX(data.x);
        command.setY(data.y);
        this.commandBus.emit('command', command);
    }
}
exports.ClickSystem = ClickSystem;
