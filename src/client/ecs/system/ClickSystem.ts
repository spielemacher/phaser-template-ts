import {Input} from 'phaser'
import {Entity, System} from 'spielemacher-ecs'
import {EventEmitter} from 'events'
import {RequestImageCreateCommand} from 'src/shared/protobuf/RequestImageCreateCommand_pb'

export class ClickSystem extends System {

  private input: Input.InputPlugin
  private commandBus: EventEmitter

  constructor(input: Input.InputPlugin, commandBus: EventEmitter) {
    super()
    this.input = input
    this.commandBus = commandBus

    this.input.on('pointerdown', this.onClick, this)
  }

  public test(entity: Entity): boolean {
    return false
  }

  public dispose() {
    this.input.removeListener('pointerdown', this.onClick, this, false)
    super.dispose()
  }

  private onClick(data: {x: number, y: number}) {
    const command = new RequestImageCreateCommand()
    command.setX(data.x)
    command.setY(data.y)
    this.commandBus.emit('command', command)
  }
}
