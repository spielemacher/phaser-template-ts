"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const spielemacher_ecs_1 = require("spielemacher-ecs");
const Command_pb_1 = require("src/shared/protobuf/Command_pb");
const RequestImageCreateCommand_pb_1 = require("src/shared/protobuf/RequestImageCreateCommand_pb");
class ServerCommunicationSystem extends spielemacher_ecs_1.System {
    constructor(socket, commandBus) {
        super();
        this.commandBus = commandBus;
        this.socket = socket;
        this.commandBus.on('command', this.onCommand.bind(this));
        this.commands = [];
        this.socket.on('request', this.onRequest.bind(this));
    }
    test(entity) {
        return false;
    }
    update() {
        let command = this.commands.shift();
        while (command) {
            this.socket.emit('command', command.serializeBinary());
            command = this.commands.shift();
        }
    }
    dispose() {
        this.commandBus.removeListener('command', this.onCommand.bind(this));
        this.socket.removeListener('request', this.onRequest.bind(this));
        super.dispose();
    }
    onCommand(command) {
        const newCommand = new Command_pb_1.Command();
        if (command instanceof RequestImageCreateCommand_pb_1.RequestImageCreateCommand) {
            newCommand.setRequestImageCreateCommand(command);
        }
        this.commands.push(newCommand);
    }
    onRequest(request) {
        this.commandBus.emit('request', request);
    }
}
exports.ServerCommunicationSystem = ServerCommunicationSystem;
