/** @typedef {events} EventEmitter */
import {System} from 'spielemacher-ecs'
import {CreateImageRequest} from 'src/shared/command/CreateImageRequest'

export class ImageSpawnSystem extends System {

  /**
   * @param eventBus EventEmitter
   */
  constructor(eventBus, addImageCallback) {
    super()
    this.eventBus = eventBus
    this.eventBus.on('request', this.onRequest.bind(this))
    this.addImageCallback = addImageCallback
  }

  onRequest(request) {
    if (request instanceof CreateImageRequest) {
      this.addImageCallback(request.x, request.y)
    }
  }

  dispose() {
    this.eventBus.removeListener('request', this.onRequest.bind(this))
    parent.dispose()
  }
}
