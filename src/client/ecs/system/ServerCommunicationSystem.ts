import {EventEmitter} from 'events'
import SocketIOClient from 'socket.io-client'
import {Entity, System} from 'spielemacher-ecs'
import {Command} from 'src/shared/protobuf/Command_pb'
import {RequestImageCreateCommand} from 'src/shared/protobuf/RequestImageCreateCommand_pb'

export class ServerCommunicationSystem extends System {

  private commandBus: EventEmitter
  private socket: SocketIOClient.Socket
  private commands: Command[]

  constructor(socket: SocketIOClient.Socket, commandBus: EventEmitter) {
    super()
    this.commandBus = commandBus
    this.socket = socket
    this.commandBus.on('command', this.onCommand.bind(this))
    this.commands = []

    this.socket.on('request', this.onRequest.bind(this))
  }

  public test(entity: Entity): boolean {
    return false
  }

  public update() {
    let command = this.commands.shift()
    while (command) {
      this.socket.emit('command', command.serializeBinary())
      command = this.commands.shift()
    }
  }

  public dispose() {
    this.commandBus.removeListener('command', this.onCommand.bind(this))
    this.socket.removeListener('request', this.onRequest.bind(this))
    super.dispose()
  }

  private onCommand(command: object) {
    const newCommand: Command = new Command()
    if (command instanceof RequestImageCreateCommand) {
      newCommand.setRequestImageCreateCommand(command)
    }
    this.commands.push(newCommand)
  }

  private onRequest(request: object) {
    this.commandBus.emit('request', request)
  }

}
