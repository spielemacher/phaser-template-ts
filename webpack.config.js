const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const DotEnvPlugin = require('dotenv-webpack')

const BUILD_PATH = path.resolve(__dirname, 'build')

module.exports = {
  mode: 'development',
  watch: true,
  entry: [
    'babel-polyfill',
    path.resolve(__dirname, 'src/client/main.ts')
  ],
  output: {
    path: BUILD_PATH,
    filename: 'phaser-template-bundle.js'
  },
  plugins: [
    new CleanWebpackPlugin([BUILD_PATH]),
    new DotEnvPlugin({
      path: './.env-dev'
    }),
    new webpack.DefinePlugin({
      'CANVAS_RENDERER': JSON.stringify(true),
      'WEBGL_RENDERER': JSON.stringify(true)
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/client/index.html')
    })
  ],
  resolve: {
    modules: [
      path.resolve(__dirname),
      'node_modules'
    ],
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      {
        test: [/\.vert$/, /\.frag$/],
        use: 'raw-loader'
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: [
          'babel-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: [
          'babel-loader'
        ]
      }
    ]
  },
  devtool: 'sourcemap',
  serve: {
    port: 8080
  }
}
